/*
Programa: CaixaAcoplada
Arquivo: valvulaAlimentacao.cpp
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "valvulaAlimentacao.h"

using namespace std;

valvulaAlimentacao::valvulaAlimentacao()
{
	cout << "Construtor da valvula de alimentacao." << endl;
}

valvulaAlimentacao::~valvulaAlimentacao()
{
	cout << "Destrutor da valvula de alimentacao." << endl;
}

float valvulaAlimentacao::getCapacidadeVazao()
{
	
	return this->capacidadeVazao;
}
