/*
Programa: CaixaAcoplada
Arquivo: comportaVedacao.cpp
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "comportaVedacao.h"

using namespace std;

comportaVedacao::comportaVedacao()
{
	cout <<"Construtor da comporta de vedacao." << endl;
}

comportaVedacao::~comportaVedacao()
{
	cout <<"Destrutor da comporta de vedacao." << endl;
}

float comportaVedacao::abrir()
{
	cout<< "Comporta de vedacao aberta. Agua saindo!!!" << endl;
	return 0;
}


void comportaVedacao::fechar()
{
	cout << "Comporta de vedacao fechada." << endl;
}
