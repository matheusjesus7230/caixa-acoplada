/*
Programa: CaixaAcoplada
Arquivo: CaixaAcoplada.h
*/
#ifndef CAIXAACOPLADA_H
#define CAIXAACOPLADA_H

#include "alavancaAcionamento.h"
#include "comportaVedacao.h"
#include "valvulaAlimentacao.h"

using namespace std;

class CaixaAcoplada
{

	private:
		
		float nivelAgua;
		const float nivelMaximo = 6.0; /*O nível máximo da caixa é 
										definido pelo fabricante.*/
		
		alavancaAcionamento *alavanca; 
		comportaVedacao *comporta;
		valvulaAlimentacao *entradaAgua; /*Uma caixa aclopada SEMPRE terá uma
										alavanca de acionamento, uma comporta de 
										vedação e uma válvula de alimentação.*/
	
	public:
		
		CaixaAcoplada();
		~CaixaAcoplada();
		
		void encherCaixa();
		void acionar();
};
#endif
