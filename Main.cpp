/*
Programa: CaixaAcoplada
Arquivo: Main.cpp
*/

#include <iostream>
#include <string>
#include "CaixaAcoplada.h"

using namespace std;

int main()
{
	CaixaAcoplada *vaso = new CaixaAcoplada(); /*Depois da caixa de descarga estiver
												pronta eu a acoplo ao vaso.*/
	vaso->acionar(); /*Dou descarga.*/
	delete vaso; /*"Desintegro" o vaso da memória do computador.*/
	
	return 0;
}
