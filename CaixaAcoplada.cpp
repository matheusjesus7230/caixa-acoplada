/*
Programa: CaixaAcoplada
Arquivo: CaixaAcoplada.cpp
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "CaixaAcoplada.h"

using namespace std;

CaixaAcoplada::CaixaAcoplada() /*Estou "construindo" a caixa*/
{
	alavanca = new alavancaAcionamento(); /*"Contruo" a alavanca, comporta e a entrada de água.*/
	comporta = new comportaVedacao();
	entradaAgua = new valvulaAlimentacao();
	
	this->nivelAgua = 0; // A caixa começa vazia.
	this->encherCaixa(); // A caixa está sendo enchida.
}

CaixaAcoplada::~CaixaAcoplada() /*Estou "me livrando" da caixa*/
{
	this->nivelAgua = 0; /*Seco a caixa*/
	
	delete alavanca; /*"Desintegro" cada componente da caixa.*/
	delete comporta;
	delete entradaAgua;
}

void CaixaAcoplada::encherCaixa() /*Vou encher a caixa.*/
{
	while(this->nivelAgua <= this->nivelMaximo) /*Encher a caixa até o seu nível máximo.*/
	{
		cout << "Nivel da agua em: " << this->nivelAgua << endl;
	
		this->nivelAgua = nivelAgua + entradaAgua->getCapacidadeVazao(); /*O nível da água irá aumentar conforme a 
																		   vazão da água que está entrando na caixa.*/
	}
}

void CaixaAcoplada::acionar() /*Vou dar a descarga.*/
{
	alavanca->acionar(); /*Aperto no botão de acionamento para dar descarga.*/
	this->nivelAgua = comporta->abrir(); /*Com a descarga aberta, a caixa é esvaziada.*/
	
	cout << "Nivel da agua em: " << this->nivelAgua << endl;
	cout << "********************************************" << endl;
	
	comporta->fechar();
	this->encherCaixa();
}
